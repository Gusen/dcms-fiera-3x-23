-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 05:03
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `nick` varchar(32) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `sess` varchar(32) DEFAULT NULL,
  `activation` varchar(32) DEFAULT NULL,
  `ban` int(11) NOT NULL DEFAULT '0',
  `ban_pr` varchar(64) DEFAULT NULL,
  `ip` bigint(20) NOT NULL DEFAULT '0',
  `ip_cl` bigint(20) NOT NULL DEFAULT '0',
  `ip_xff` bigint(20) NOT NULL DEFAULT '0',
  `ua` varchar(32) DEFAULT NULL,
  `date_reg` int(11) NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `date_aut` int(11) NOT NULL DEFAULT '0',
  `date_last` int(11) NOT NULL DEFAULT '0',
  `balls` int(11) NOT NULL DEFAULT '0',
  `rating` varchar(99) NOT NULL DEFAULT '0.00',
  `level` enum('0','1','2','3','4') NOT NULL DEFAULT '0',
  `group_access` int(10) unsigned NOT NULL DEFAULT '0',
  `pol` enum('0','1') NOT NULL DEFAULT '1',
  `url` varchar(64) NOT NULL DEFAULT '/',
  `show_url` enum('0','1') NOT NULL DEFAULT '1',
  `ank_g_r` int(4) DEFAULT NULL,
  `ank_m_r` int(2) DEFAULT NULL,
  `ank_d_r` int(2) DEFAULT NULL,
  `ank_city` varchar(32) DEFAULT NULL,
  `ank_o_sebe` varchar(512) DEFAULT NULL,
  `ank_icq` int(9) DEFAULT NULL,
  `ank_skype` varchar(32) DEFAULT NULL,
  `ank_mail` varchar(32) DEFAULT NULL,
  `ank_n_tel` varchar(11) DEFAULT NULL,
  `ank_name` varchar(32) DEFAULT NULL,
  `set_time_chat` int(11) DEFAULT '30',
  `set_p_str` int(11) DEFAULT '10',
  `set_show_icon` set('0','1','2') DEFAULT '1',
  `set_translit` enum('0','1') NOT NULL DEFAULT '1',
  `set_files` enum('0','1') NOT NULL DEFAULT '1',
  `set_timesdvig` int(11) NOT NULL DEFAULT '0',
  `set_news_to_mail` enum('0','1') NOT NULL DEFAULT '0',
  `set_show_mail` enum('0','1') NOT NULL DEFAULT '0',
  `set_them` varchar(32) DEFAULT 'default',
  `set_them2` varchar(32) DEFAULT 'default',
  `meteo_country` int(11) NOT NULL DEFAULT '0',
  `autorization` enum('0','1') NOT NULL DEFAULT '0',
  `add_konts` enum('0','1','2') NOT NULL DEFAULT '2',
  `status_ank` varchar(255) NOT NULL DEFAULT '',
  `news_count` int(11) DEFAULT '1',
  `lang` varchar(32) DEFAULT 'ru',
  `money` bigint(11) DEFAULT '0',
  `anketa_search` int(11) DEFAULT '1',
  `mylink` varchar(99) DEFAULT NULL,
  `browser` varchar(99) DEFAULT 'wap',
  `rating_tmp` varchar(99) DEFAULT '0.00',
  `flood_time` int(11) DEFAULT '0',
  `flood_count` int(11) DEFAULT '0',
  `no_repass` int(11) DEFAULT '0',
  `tester` int(11) DEFAULT '0',
  `url_title` varchar(99) DEFAULT '',
  `pass_time` int(11) DEFAULT '0',
  `license_time` int(11) DEFAULT '0',
  `ank_countr` varchar(99) DEFAULT '',
  `ank_family` varchar(99) DEFAULT '',
  `soc_nick` int(11) DEFAULT '0',
  `foto_ava_set` int(11) DEFAULT '1',
  `forum_url` varchar(100) NOT NULL DEFAULT '',
  `hash` varchar(99) DEFAULT NULL,
  `hash_set` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nick` (`nick`), ADD KEY `url` (`url`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
