<?php

foreach (array(
    'start',
    'home',
    'compress',
    'sess',
    'settings',
    'db_connect',
    'ipua',
    'fnc',
    'user',
    'icons',
    'thead') as $inc)
{
    require_once 'sys/inc/' . $inc . '.php';
}
require_once ('pages/plugins/controller_m_menu.php');
require_once ('sys/inc/tfoot.php');
